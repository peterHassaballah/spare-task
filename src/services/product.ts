import { IProduct } from '../types/product.interface';
import Product from '../models/product'; 
import ApiError from '../utils/ApiError';

export const getProducts = async (): Promise<IProduct[]> => {
  try {
    const products = await Product.find();
    return products;
  } catch (error) {
    throw new ApiError(500, 'Failed to fetch products');
  }
}

export const getProductById = async (productId: string): Promise<IProduct | null> => {
  try {
    const product = await Product.findById(productId);
    if (!product) {
        throw new ApiError(404, 'No matching product found',false);
      }
    return product;
  } catch (error) {
    throw new ApiError(500, 'Failed to fetch product by ID');
  }
}

export const addProduct = async (payload: IProduct): Promise<IProduct> => {
  try {
    const newProduct = await Product.create(payload);
    return newProduct;
  } catch (error) {
    throw new ApiError(400, 'Failed to create product');
  }
}

export const getMyProducts = async (userId: string): Promise<IProduct[]> => {
  try {
    const products = await Product.find({ userId });
    return products;
  } catch (error) {
    throw new ApiError(500, 'Failed to fetch user products');
  }
}

export const editProduct = async (productId: string, data: any): Promise<IProduct | null> => {
  try {
    const updatedProduct = await Product.findByIdAndUpdate(productId, data, { new: true });
    if (!updatedProduct) {
        throw new ApiError(404, 'Product not found, revise product Id',false);
      }
    return updatedProduct;
  } catch (error) {
    throw new ApiError(500, 'Failed to update product');
  }
}

export const removeProduct = async (productId: string): Promise<void> => {
  try {
    await Product.findByIdAndDelete(productId);
  } catch (error) {
    throw new Error('Failed to delete product');
  }
}
