import { ICart } from "../types/cart.interface";
import Cart from '../models/Cart';
import Product from "../models/product";
import ApiError from "../utils/ApiError";

export const updateCart = async (user_id: string, payload: any) => {
  const { productId, quantity } = payload;
  const availFlag = await checkItemAvailabilityAndUpdate(productId, quantity);
  if (!availFlag) {
    throw new ApiError(403, "Requested quantity is not available", false);
  }
  let cart = await Cart.findOne({ user_id });
  if (!cart) {
    cart = await createCart(user_id);
  }
  let existingItemIndex = -1
  if (cart.items.length > 0) {
    // Check if the product is already in the cart
    existingItemIndex = cart.items.findIndex(item => item.productId.toString() === productId);
    if (existingItemIndex !== -1) {
      // If the product exists in the cart, update its quantity and price
      const oldQuantity = cart.items[existingItemIndex].quantity;
      const oldPrice = Number(cart.items[existingItemIndex].price) / oldQuantity;

      // Perform the addition operation ensuring compatibility with the decimal type
      const newQuantity = oldQuantity + quantity;
      const newPrice = (quantity * oldPrice);

      cart.items[existingItemIndex].quantity = newQuantity;
      cart.items[existingItemIndex].price = newPrice;
    }
  }

  if (existingItemIndex === -1 || cart.items.length === 0) {
    // If the product doesn't exist in the cart, add it
    const product = await Product.findOne({ _id: productId }).lean();
    cart.items.push({ productId, quantity, price: product.price });
  }
  await cart.save();
  return cart;
}
export const createCart = async (user_id: string): Promise<ICart | null> => {
  return await new Cart({ user_id, items: [] });
}
export const removeCart = async (user_id: string) => {
  try {
    // Find the cart associated with the provided user_id
    const cart = await Cart.findOneAndDelete({ user_id });

    if (!cart) {
      throw new Error('Cart not found');
    }

    // Retrieve all the products in the cart
    const items = cart.items;

    // Increment the quantity of each product in the Product model
    await Promise.all(items.map(async (item) => {
      const product = await Product.findById(item.productId);
      if (product) {
        product.quantity += item.quantity; // Increment the quantity by the quantity in the cart item
        await product.save();
      }
    }));

    return cart;
  } catch (error:any) {
    throw new ApiError(400,`Failed to delete cart: ${error?.message}`);
  }
}
export const getCart = async (user_id: string): Promise<ICart | null> => {
  return await Cart.findOne({ user_id });
}

const checkItemAvailabilityAndUpdate = async (productId: string, requiredQuantity: number): Promise<boolean> => {
  try {
    // Fetch the product from the database
    const product = await Product.findById(productId);

    if (!product || product?.status === 'NOT AVAILABLE') {
      // Product not found
      return false;
    }

    // Check if the available quantity is greater than or equal to the required quantity
    if (product.quantity >= requiredQuantity) {
      // Update the quantity in the Product model
      product.quantity -= requiredQuantity;

      // If the new quantity is 0, update the status of the item
      if (product.quantity === 0) {
        product.status = 'NOT AVAILABLE';
      }

      // Save the updated product
      await product.save();

      return true;
    } else {
      // Product quantity is insufficient
      return false;
    }
  } catch (error) {
    console.error('Error checking item availability and updating:', error);
    return false;
  }
};
