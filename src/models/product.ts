import mongoose from 'mongoose';
import { IProduct } from '../types/product.interface';
const productSchema = new mongoose.Schema(
    {
      product_title_en: {
        type: String,
        // required: true,
        trim: true,
      },
      product_title_ar: {
        type: String,
        // required: true,
        trim: true,
      },
      product_id: {
        type: String,
        trim: true,
      },
      status: {
        type: String,
        enum: [ "AVAILABLE", "NOT AVAILABLE"],
        default: "AVAILABLE",
      },
      price: {
        // to accomdate double in pricing
        type: mongoose.Schema.Types.Decimal128,
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
        default: 1,
      },
      currency: {
        type: String,
        enum: ["EGP", "USD", "SAR"],
      },
      main_img_url: {
        type: [String],
        default:["https://assets-global.website-files.com/63cd3650993f00573d51a5af/63cefa8c25a9111242c0b690_logo.svg"]
      },
      
      description:{
        type:String,
      },
    },
    {
      timestamps: true,
    }
  );
  
  
const Product = mongoose.model<IProduct>('Product', productSchema);

export default Product;