import mongoose from 'mongoose';
import { ICart } from '../types/cart.interface';
const cartSchema = new mongoose.Schema({
  user_id: {
    // type: mongoose.Schema.Types.ObjectId,
    // ref: 'User',
    type:String,
    required: true,
  },
  items: [{
    productId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product',
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
      default: 1,
    },
    price: {
      type: Number,
      required: true,
    }
  },],
}, {
  timestamps: true,
});

const Cart = mongoose.model<ICart>('Cart', cartSchema);

export default Cart;
