import { Router } from "express";
import productRoute from './products';
import cartRoute from './carts';
const router = Router();
const defaultRoutes=[
  {
    path: "/product",
    route: productRoute,
  },
  {
    path: "/cart",
    route: cartRoute,
  },
];
defaultRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
// export default router;
export { router as indexRoute };