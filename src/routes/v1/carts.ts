import { Router } from "express";
import validate from "../../middlewares/validate";
import { createCart, deleteCart } from "../../validations/cart";
import {
    addToCart,
    getCartDetails,
    deleteCart as _deleteCart
} from "../../controllers/cart";
const router = Router();
//routes to get all/create carts
router
    .route("/")
    .post(validate(createCart), addToCart)
// get user's carts (shopping list)
router.get('/:userId/list', getCartDetails);

router.delete('/deleteCart', validate(deleteCart), _deleteCart);

export default router;