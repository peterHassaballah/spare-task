import { Router } from "express";
import validate from "../../middlewares/validate";
import { createProduct, updateProduct } from "../../validations/product";
import {
    createProduct as _createProduct,
    getAllProducts as _getProducts,
    getProduct as _getProduct,
    updateProduct as _updateProduct,
    getUserProducts
} from "../../controllers/product";
const router = Router();
//routes to get all/create products
router
    .route("/")
    .post(validate(createProduct), _createProduct)
    .get(_getProducts);
// get user's products (shopping list)
router.get('/list', getUserProducts);
// update/get product details
router
    .route("/:productId")
    .get(_getProduct)
    .patch(validate(updateProduct), _updateProduct);

//export authRoute
export default router;