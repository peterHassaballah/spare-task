import { Document, Types } from 'mongoose';

interface CartItem {
  productId: Types.ObjectId;
  quantity: number;
  price: Types.Decimal128 | Number | number;
}

export interface ICart extends Document {
  user_id: Types.ObjectId;
  items: CartItem[];
  createdAt?: Date;
  updatedAt?: Date;
}