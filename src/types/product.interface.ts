import { Document, Types } from 'mongoose';
export interface IProduct extends Document {
  product_title_en: string;
  product_title_ar: string;
  product_id?: string;
  status: "AVAILABLE" | "NOT AVAILABLE";
  price: Types.Decimal128 | Number | number;
  quantity: number;
  currency?: "EGP" | "USD" | "SAR";
  main_img_url: string[];
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
}
