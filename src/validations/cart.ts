import joi from "joi";

export const createCart = {
    body: joi.object().keys({
        user_id: joi.string().required(),
        productId: joi.string().required(),
        quantity: joi.number().required(),
        price: joi.number(),
    }),
};

export const deleteCart = {
    body: joi.object().keys({
        user_id: joi.string().required(),
    }),
};
