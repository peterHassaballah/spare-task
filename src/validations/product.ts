import joi from "joi";
import { objectId } from "./custom";

export const createProduct = {
    body: joi
      .object()
      .keys({
        product_title_en: joi.string().required(),
        product_title_ar: joi.string().required(),
        status: joi.string().valid("AVAILABLE", "NOT AVAILABLE").required(),
        price: joi.number().required(),
        quantity: joi.number().required(),
        currency: joi.string().valid("EGP", "USD", "SAR"),
        main_img_url: joi.array().items(joi.string()).required(),
        description: joi.string(),
      })
      .min(1),
};

export const updateProduct = {
    params: joi.object().keys({
      productId: joi.required().custom(objectId),
    }),
    body: joi
      .object()
      .keys({
        product_title_en: joi.string(),
        product_title_ar: joi.string(),
        status: joi.string().valid("AVAILABLE", "NOT AVAILABLE"),
        price: joi.number(),
        quantity: joi.number(),
        currency: joi.string().valid("EGP", "USD", "SAR"),
        main_img_url: joi.array().items(joi.string()),
        description: joi.string(),
      })
      .min(1),
};
