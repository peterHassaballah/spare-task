import { Request, Response } from 'express';
import {
    updateCart,
    removeCart,
    getCart
} from '../services/cart';
// Add to cart
export const addToCart = async (req: Request, res: Response): Promise<Response> => {
  const { user_id} = req.body;

  try {
    const updated = await updateCart(user_id,req.body);
    if(!updated){
      return res.status(400).json({ message: 'Cannot add to cart' });
    }
    return res.status(200).json(updated);
  } catch (error) {
    console.log("error caught adding to cart",error)
    return res.status(500).json({ message: 'Internal server error' });
  }
};

// Get cart details
export const getCartDetails = async (req: Request, res: Response): Promise<Response> => {
  const { userId } = req.params;

  try {
    const cart = await getCart(userId);
    if (!cart) {
      return res.status(404).json({ message: 'Cart not found' });
    }
    return res.status(200).json(cart);
  } catch (error) {
    console.log("error caught getting cart details",error)
    return res.status(500).json({ message: 'Internal server error' });
  }
};

// Delete cart
export const deleteCart = async (req: Request, res: Response): Promise<Response> => {
  const { user_id } = req.body;

  try {
    const cart = await removeCart(user_id);
    if (!cart) {
      return res.status(404).json({ message: 'Cart not found' });
    }

    return res.status(204).end();
  } catch (error) {
    console.log("error caught deleting cart",error)
    return res.status(500).json({ message: error});
  }
};
