import { RequestHandler,Response,Request } from 'express';
import {
  getProductById,
  addProduct,
  getProducts,
  getMyProducts,
  editProduct,
  removeProduct
} from '../services/product';

export const getAllProducts: RequestHandler = async (req, res, next) => {
  try {
    const products = await getProducts();
    return res.status(200).json(products);
  } catch (error) {
    return res.status(500).json({ message: 'Internal server error' });
  }
};
export const getProduct:RequestHandler = async (req, res,next) => {
    try {
    const productId = req.params.productId;
    const product = await getProductById(productId);
      if(!product){
          return res.status(404).json({'message':'No matching Id found'});
      }
      return res.status(200).json(product);
    } catch (error) {
      return res.status(500).json({ message: 'Internal server error' });
    }
  };

export const createProduct = async (req:Request, res:Response) => {
  try {
    const payload=req.body;
    const newProduct = await addProduct(payload);
    if(!newProduct){      
      return res.status(400).json({ message: 'Failed to create product' });
    }
    return res.status(201).json(newProduct);
  } catch (error:any) {
    console.log("the error",error);
    return res.status(400).json({ message: `Error in Productcreation: ${error.message}` });
  }
};
export const getUserProducts = async (req:Request, res:Response) => {
  try {
    const {userId} = req.params;
    const products = await getMyProducts(userId);
    return res.status(200).json(products);
  } catch (error:any) {
    console.log("the error",error);
    return res.status(500).json({ message: `Cannot get user products ${error?.message}` });
  }
};

export const updateProduct = async (req:Request, res:Response) => {
  const productId = req.params.productId;
  try {
    const updatedProduct = await editProduct(productId, req.body);
    if(!updatedProduct){
      return res.status(404).json({'message':'No matching Id found'});
    }    
    return res.json(updatedProduct);
  } catch (error) {
    return res.status(400).json({ message: 'Failed to update product' });
  }
};


export const deleteProduct: RequestHandler = async (req, res) => {
  const productId = req.params.productId;
  try {
    await removeProduct(productId);
    return res.status(204).end();
  } catch (error) {
    return res.status(400).json({ message: 'Failed to delete product' });
  }
};
