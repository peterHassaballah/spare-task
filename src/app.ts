import express, { json, urlencoded,Response,NextFunction,Request } from "express";
import bodyParser from "body-parser";
import compression from "compression";
import cors from "cors";
import dotenv from 'dotenv';
dotenv.config();
import { indexRoute } from "./routes/v1/index";
console.log("hello world we are here:",process.env.PORT)
const app = express();
// Socket.io server

app.use((req:Request, res:Response, next:NextFunction) => {
  next();
});
// parse json request body
app.use(json());
// gzip compression
app.use(compression());
// body parser
app.use(bodyParser.urlencoded({extended:true}));
// enable cors
app.use(cors());
// parse urlencoded request body
app.use(urlencoded({ extended: true }));
// v1 api routes
app.use("/v1", indexRoute);

// send back a 404 error for any unknown api request
app.use((req:Request, res:Response, next:NextFunction) => {
    next(res.status(404).send({ msg: "Not found" }));
});
// convert error to ApiError, if needed
// app.use(errorConverter);

//  handle error
// app.use(errorHandler);

export default app;