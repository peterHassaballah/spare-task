# spare-task

## Overview

Brief description of your project.

## API Specification

### Add to Cart

Endpoint: `/v1/cart/`  
Method: `POST`  
Description: Adds items to the user's cart or updates the quantity if the item is already in the cart.  
Required body:

- `user_id`: The ID of the user.
- `products`: An array of objects containing the following fields:
  - `productId`: The ID of the product.
  - `quantity`: The quantity of the product to add/update.
  - `price`: The price of the product.

### Get Cart Details

Endpoint: `/v1/:userId/list`  
Method: `GET`  
Description: Retrieves the details of the user's cart.  
Required Parameters:

- `user_id`: The ID of the user.

### Delete Cart

Endpoint: `/v1/deleteCart`  
Method: `DELETE`  
Description: Deletes the user's cart.  
Required body:

- `user_id`: The ID of the user.

## Build Instructions

To get the app running, follow these steps:

1. Clone the repository:

2. Navigate to the project directory:

3. Install dependencies:

4. Compile to Build:

```bash
npm run build
```

5. Start the server:

```bash
npm run dev
```

The url to use the application is  `http://localhost:5000`